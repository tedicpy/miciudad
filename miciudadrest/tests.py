import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Comment, LocatableByGeoPoint, Commentable, Event
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_comment(**kwargs):
    defaults = {}
    defaults["text"] = "text"
    defaults.update(**kwargs)
    if "author" not in defaults:
        defaults["author"] = create_comment()
    return Comment.objects.create(**defaults)


def create_locatablebygeopoint(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return LocatableByGeoPoint.objects.create(**defaults)


def create_commentable(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    if "comment" not in defaults:
        defaults["comment"] = create_comment()
    return Commentable.objects.create(**defaults)


def create_event(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults.update(**kwargs)
    return Event.objects.create(**defaults)


class CommentViewTest(unittest.TestCase):
    '''
    Tests for Comment
    '''
    def setUp(self):
        self.client = Client()

    def test_list_comment(self):
        url = reverse('framework_comment_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_comment(self):
        url = reverse('framework_comment_create')
        data = {
            "text": "text",
            "author": create_comment().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_comment(self):
        comment = create_comment()
        url = reverse('framework_comment_detail', args=[comment.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_comment(self):
        comment = create_comment()
        data = {
            "text": "text",
            "author": create_comment().pk,
        }
        url = reverse('framework_comment_update', args=[comment.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LocatableByGeoPointViewTest(unittest.TestCase):
    '''
    Tests for LocatableByGeoPoint
    '''
    def setUp(self):
        self.client = Client()

    def test_list_locatablebygeopoint(self):
        url = reverse('framework_locatablebygeopoint_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_locatablebygeopoint(self):
        url = reverse('framework_locatablebygeopoint_create')
        data = {
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_locatablebygeopoint(self):
        locatablebygeopoint = create_locatablebygeopoint()
        url = reverse('framework_locatablebygeopoint_detail', args=[locatablebygeopoint.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_locatablebygeopoint(self):
        locatablebygeopoint = create_locatablebygeopoint()
        data = {
        }
        url = reverse('framework_locatablebygeopoint_update', args=[locatablebygeopoint.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CommentableViewTest(unittest.TestCase):
    '''
    Tests for Commentable
    '''
    def setUp(self):
        self.client = Client()

    def test_list_commentable(self):
        url = reverse('framework_commentable_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_commentable(self):
        url = reverse('framework_commentable_create')
        data = {
            "comment": create_comment().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_commentable(self):
        commentable = create_commentable()
        url = reverse('framework_commentable_detail', args=[commentable.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_commentable(self):
        commentable = create_commentable()
        data = {
            "comment": create_comment().pk,
        }
        url = reverse('framework_commentable_update', args=[commentable.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class EventViewTest(unittest.TestCase):
    '''
    Tests for Event
    '''
    def setUp(self):
        self.client = Client()

    def test_list_event(self):
        url = reverse('framework_event_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_event(self):
        url = reverse('framework_event_create')
        data = {
            "name": "name",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_event(self):
        event = create_event()
        url = reverse('framework_event_detail', args=[event.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_event(self):
        event = create_event()
        data = {
            "name": "name",
        }
        url = reverse('framework_event_update', args=[event.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)



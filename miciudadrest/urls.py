from django.conf.urls import url, include
from rest_framework import routers
from . import api

router = routers.DefaultRouter()
router.register(r'event', api.EventViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    url(r'^v1/', include(router.urls)), 
)





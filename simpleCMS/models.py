import datetime
from django.db import models
from django.contrib.gis.db import models as gismodels
from django.contrib.auth.models import User
from django.contrib.gis.serializers import geojson


class DateMixin(models.Model):
    date_added = models.DateTimeField(default=datetime.datetime.now)
    date_modified = models.DateTimeField()

    def save(self):
        self.date_modified = datetime.datetime.now()
        super(DateMixin, self).save()

class Comment(models.Model):
    text = models.TextField(max_length=100)
    author = models.ForeignKey(User, help_text="The comment's author")

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return '%s' % self.pk

    def get_absolute_url(self):
        return reverse('framework_comment_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('framework_comment_update', args=(self.pk,))

class CommentMixin(models.Model):
    """ A mixin to add a comment to any model """
    comment = models.ForeignKey(Comment, )

    class Meta:
        abstract = True

class Tag(models.Model):
    """ Clase para rempresentar una etiqueta. Pienso ponerle jerarquías """
    tag = models.CharField(blank=True, max_length=100)

    def __str__(self):
        return "%s" % self.tag

class TagMixin(models.Model):
    tags = models.ManyToManyField(Tag, blank=True)

    class Meta:
        abstract = True

class LocationPointMixin(models.Model):
    """ A mixin for add a a geo location point to any model """
    point = gismodels.PointField()

    class Meta:
        abstract = True

from django.db import models
from simpleCMS.models import LocationPointMixin, TagMixin
from django.core.urlresolvers import reverse

# Create your models here.

class Base(models.Model):
    """(Base class for manage common elements)"""

    def __str__(self):
        return "Base"


class Event(Base, LocationPointMixin, TagMixin):
    title = models.CharField(max_length=50)
    # image = models.ImageField(upload_to="/dir/path", height_field=, width_field=)
    direccion = models.CharField(blank=True, max_length=100)
    # fecha y hora =

    def get_absolute_url(self):
        return reverse('event-detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('event-edit', args=(self.pk,))

    def __str__(self):
        return '%s en %s' % (self.title, self.direccion)

# from django.shortcuts import render
from django.views.generic import ListView, CreateView, DetailView, UpdateView, TemplateView
from miciudadapp.models import Event, Base
from .forms import EventForm
from .serializers import event_serializer

from django.core.serializers import serialize


# class PublisherList(ListView):
#     model = Event
#     context_object_name = 'event'
#     # queryset = Event.objects.all()
#     template_name = 'books/acme_list.html'

class EventListView(ListView):
    model = Event


class EventCreateView(CreateView):
    model = Event
    form_class = EventForm

class EventFormView(CreateView):
    model = Event
    form_class = EventForm
    template_name = "form.html"


class EventDetailView(DetailView):
    model = Event


class EventUpdateView(UpdateView):
    model = Event
    form_class = EventForm
    template_name = "miciudadapp/event_detail.html"



class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['object_list'] = Event.objects.all()
        context['geojson'] = serialize('geojson', Event.objects.all(), geometry_field='point', fields=('title', 'direccion'))
        return context

from django import forms
from miciudadapp.models import Event, Base
from leaflet.forms.widgets import LeafletWidget


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'direccion', 'tags', 'point']
        widgets = {'point': LeafletWidget()}

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            # if hasattr(field, 'geom_type'):
            #     field.widget.template_name = 'map.html'

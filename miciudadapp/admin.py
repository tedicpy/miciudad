from django.contrib import admin
from django.contrib.gis import admin as geoadmin
from leaflet.admin import LeafletGeoAdmin
from .models import Base, Event
from simpleCMS.models import Tag

# Register your models here.
# geoadmin.site.register(Event)
geoadmin.site.register(Tag)
admin.site.register(Event, LeafletGeoAdmin)

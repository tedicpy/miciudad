from django.core.serializers import serialize
from miciudadapp.models import Event


def event_serializer():
    serialize('geojson', Event.objects.all(), geometry_field='point', fields=('title', 'direccion'))
